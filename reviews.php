<?php 
include ('elements/header.php');
?>
        <section class="contentWrapper">
            <!-- [LEFT SIDE MENU] -->
            <aside>
                <nav class="menuBar">
                    <li><a href="#">НОВИНКИ</a></li>
                    <li><a href="#">ЛУЧШИЕ ПРЕДЛОЖЕНИЯ</a></li>
                    <li><a href="#">РАСПРОДАЖА</a></li>

                    <li><a href="#">Шапки</a></li>
                    <li>
                        <a href="#" class="menuBar__more">Шарфы и платки</a>
                        <ul>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Солнцезащитные платки</a></li>
                    <li><a href="#">Ремни</a></li>
                    <li><a href="#">Часы</a></li>
                    <li><a href="#">Кошельки</a></li>
                    <li><a href="#">Перчатки</a></li>
                    <li><a href="#">Зонты</a></li>
                    <li>
                        <a href="#" class="menuBar__more">Для волос</a>
                        <ul>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="menuBar__more">Украшения</a>
                        <ul>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="menuBar__more">Пляжные аксессуары</a>
                        <ul>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Носки</a></li>
                    <li><a href="#">Домашняя обувь</a></li>
                    <li><a href="#">Маски карнавальные</a></li>

                    <li>
                        <a href="#" class="menuBar__more">ДЕТЯМ</a>
                        <ul>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="menuBar__more">МУЖЧИНАМ</a>
                        <ul>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                            <li><a href="#">Подпункт меню</a></li>
                        </ul>
                    </li>
                </nav>
            </aside>
            <!-- [/END MENU] -->

            <!-- [RIGHT SIDE] -->
            <section class="mainContent">
                <!-- [Head] -->
                <nav class="minLinks">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Отзывы</a></li>
                </nav>
                <h1>Отзывы</h1>
                <!-- [/End Head] -->

                <!-- [MAIN CONTENT] -->
                <main class="reviewsPage">
                    <figure class="orangeBlock">
                        <img src="img/reviewsGirl.png" alt="">
                        <div class="orangeBlock__wrapper">
                            <figcaption>
                                <h2>Помогите нам<br>стать лучше!</h2>
                                <span>
                                    Напишите, что вы заказывали,<br>
                                    понравился ли вам товар,<br>
                                    удобно ли было заказывать.
                                </span>
                                <a href="#">Написать отзыв</a>
                            </figcaption>
                        </div>
                    </figure>

                    <section class="reviewsPage__wrapper">
                        <article>
                            <img src="img/noPhoto.jpg" alt="">
                            <section class="reviewContent">
                                <h3>Анастасия Иванова</h3>
                                <span class="reviewContent__uHead">10 сентября 2018  |  Киров</span>
                                <span class="reviewContent__text">
                                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fugiat natus animi soluta in fugit iste laudantium suscipit reiciendis autem porro, qui quos repellendus esse voluptates quas rerum! Corrupti, quisquam omnis!
                                </span>
                                <a href="#">Читать дальше</a>
                            </section>
                        </article>
                        <article>
                            <img src="uploads/avaGirl.jpg" alt="">
                            <section class="reviewContent">
                                <h3>Анастасия Иванова</h3>
                                <span class="reviewContent__uHead">10 сентября 2018  |  Киров</span>
                                <span class="reviewContent__text">
                                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fugiat natus animi soluta in fugit iste laudantium suscipit reiciendis autem porro, qui quos repellendus esse voluptates quas rerum! Corrupti, quisquam omnis!
                                    <div class="attachFiles">
                                        <img src="uploads/attachPhotoReview.jpg" alt="">
                                        <img src="uploads/attachPhotoReview.jpg" alt="">
                                    </div>
                                </span>
                            </section>
                        </article>
                        <article>
                            <img src="img/noPhoto.jpg" alt="">
                            <section class="reviewContent">
                                <h3>Анастасия Иванова</h3>
                                <span class="reviewContent__uHead">10 сентября 2018  |  Киров</span>
                                <span class="reviewContent__text">
                                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fugiat natus animi soluta in fugit iste laudantium suscipit reiciendis autem porro, qui quos repellendus esse voluptates quas rerum! Corrupti, quisquam omnis!
                                </span>
                                <a href="#">Читать дальше</a>
                            </section>
                        </article>
                    </section>

                    <button class="showMore">Показать ещё</button>
                </main>
                <!-- [/END CONTENT] -->
            </section>
            <!-- [/END RIGHT] -->
        </section>
    </div>
    
    <!-- [FOOTER] -->
    <footer>
        <div class="footer__wrapper">
            <section class="footer__top">
                <nav>
                    <li><a href="#">Как заказать</a></li>
                    <li><a href="#">Бонусная программа</a></li>
                    <li><a href="#">Оплата и доставка</a></li>
                    <li><a href="#">Гарантии и возврат</a></li>
                    <li><a href="#">Вопрос-ответ</a></li>
                </nav>
                <nav>
                    <li><a href="#">О компании</a></li>
                    <li><a href="#">Отзывы</a></li>
                    <li><a href="#">Франшиза</a></li>
                    <li><a href="#">Контакты</a></li>
                </nav>
                <section class="footer__contacts">
                    <ul class="number">
                        <li><i class="fas fa-phone-alt"></i>8-888-888-88-88</li>
                        <li><i class="fas fa-envelope"></i>frimis@gmail.com</li>
                    </ul>
                    <ul class="social">
                        <li><a href="#"><i class="fab fa-vk"></i></a></li>
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fab fa-odnoklassniki"></i></a></li>
                    </ul>
                </section>
                <form action="#">
                    <h4>Оформите подписку</h4>
                    <input type="text" placeholder="Укажите e-mail">
                    <input type="submit" value="Подписаться">
                    <label>
                        Нажимая на кнопку «Подписаться», я
                        соглашаюсь на обработку моих персональных
                        данных и ознакомлен(а) с условиями
                        конфиденциальности.
                    </label>
                </form>
            </section>
            <section class="footer__info">
                <span>
                    © «Frimis» — интернет-магазин украшений и аксессуаров.<br>
                    <a href="#">Политика конфиденциальности.</a>
                </span>
                <a href="#" class="fiveLogo">Разработка<br>и дизайн сайта «FIVE»</a>
            </section>
        </input>
    </footer>
    <!-- [/END FOOTER] -->

    <!-- [SCRIPTS] -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="libs/owlcarousel/owl.carousel.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
    /* == [SELECT UI] == */
    $('.chLook__list').selectmenu({
        classes: {
            'ui-selectmenu-button-closed': 'selectMenu_closed',
            'ui-selectmenu-button-open': 'selectMenu_open',
            'ui-selectmenu-menu': 'selectMenu__menu'
        }
    });
    </script>

    <script src="js/main.js"></script>
    <!-- [/SCRIPTS] -->
</body>
</html>